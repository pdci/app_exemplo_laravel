<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('enterprises','EnterpriseController');
Route::resource('technicalTeam','TechnicalTeamController');

Route::resource('process','ProcessController');
Route::resource('documents','DocumentController');
Route::resource('reports','ReportController');
Route::resource('maps','MapController');
Route::resource('tables','TableController');
Route::resource('progress','ProcessProgressController');
Route::resource('queries', 'QueryController');
Route::resource('sislic', 'SislicController');


// create params
Route::get('/createprocess/{id}', 'ProcessController@create');
Route::get('/addanalyst/{id}', 'TechnicalTeamController@create');
Route::get('/createdocument/{id}', 'DocumentController@create');


//post
Route::get('/newrole', 'TechnicalTeamController@newRole');

// downloads
Route::get('/dd/{id}', 'DocumentController@download')->name('dd');
Route::get('/dt/{id}', 'TableController@download')->name('dt');
Route::get('/dm/{id}', 'MapController@download')->name('dm');
