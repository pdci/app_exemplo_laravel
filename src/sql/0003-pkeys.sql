create sequence lafsisbio.lafsisbio_analyst_id_seq owned by lafsisbio.analyst.id;
create sequence lafsisbio.lafsisbio_analysts_id_seq owned by lafsisbio.analysts.id;
create sequence lafsisbio.lafsisbio_area_protegidas_id_seq owned by lafsisbio.area_protegidas.id;
create sequence lafsisbio.lafsisbio_cavernas_id_seq owned by lafsisbio.cavernas.id;
create sequence lafsisbio.lafsisbio_documents_id_seq owned by lafsisbio.documents.id;
create sequence lafsisbio.lafsisbio_enterprises_id_seq owned by lafsisbio.enterprises.id;
create sequence lafsisbio.lafsisbio_permissions_id_seq owned by lafsisbio.permissions.id;
create sequence lafsisbio.lafsisbio_processes_id_seq owned by lafsisbio.processes.id;
create sequence lafsisbio.lafsisbio_reports_documents_id_seq owned by lafsisbio.reports_documents.id;
create sequence lafsisbio.lafsisbio_roles_id_seq owned by lafsisbio.roles.id;
create sequence lafsisbio.lafsisbio_technical_team_id_seq owned by lafsisbio.technical_team.id;
create sequence lafsisbio.lafsisbio_users_id_seq owned by lafsisbio.users.id;

alter table lafsisbio.analyst alter column id set default nextval('lafsisbio.lafsisbio_analyst_id_seq');
alter table lafsisbio.analysts alter column id set default nextval('lafsisbio.lafsisbio_analysts_id_seq');
alter table lafsisbio.area_protegidas alter column id set default nextval('lafsisbio.lafsisbio_area_protegidas_id_seq');
alter table lafsisbio.cavernas alter column id set default nextval('lafsisbio.lafsisbio_cavernas_id_seq');
alter table lafsisbio.documents alter column id set default nextval('lafsisbio.lafsisbio_documents_id_seq');
alter table lafsisbio.enterprises alter column id set default nextval('lafsisbio.lafsisbio_enterprises_id_seq');
alter table lafsisbio.permissions alter column id set default nextval('lafsisbio.lafsisbio_permissions_id_seq');
alter table lafsisbio.processes alter column id set default nextval('lafsisbio.lafsisbio_processes_id_seq');
alter table lafsisbio.reports_documents alter column id set default nextval('lafsisbio.lafsisbio_reports_documents_id_seq');
alter table lafsisbio.roles alter column id set default nextval('lafsisbio.lafsisbio_roles_id_seq');
alter table lafsisbio.technical_team alter column id set default nextval('lafsisbio.lafsisbio_technical_team_id_seq');
alter table lafsisbio.users alter column id set default nextval('lafsisbio.lafsisbio_users_id_seq');


alter table lafsisbio.processes alter column others_text drop not null;
