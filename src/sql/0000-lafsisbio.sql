﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 11.0
-- Dumped by pg_dump version 11.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: lafsisbio; Type: SCHEMA; Schema: -; Owner: alfredo
--
SET default_tablespace = '';

SET default_with_oids = false;


DROP TABLE IF EXISTS lafsisbio.analyst;
DROP TABLE IF EXISTS lafsisbio.analysts;
DROP TABLE IF EXISTS lafsisbio.area_protegidas;
DROP TABLE IF EXISTS lafsisbio.cavernas;
DROP TABLE IF EXISTS lafsisbio.documents;
DROP TABLE IF EXISTS lafsisbio.enterprises;
DROP TABLE IF EXISTS lafsisbio.enterprises_processes;
DROP TABLE IF EXISTS lafsisbio.enterprises_technicals;
DROP TABLE IF EXISTS lafsisbio.password_resets;
DROP TABLE IF EXISTS lafsisbio.permissions;
DROP TABLE IF EXISTS lafsisbio.processes;
DROP TABLE IF EXISTS lafsisbio.processes_analysts;
DROP TABLE IF EXISTS lafsisbio.processes_areas;
DROP TABLE IF EXISTS lafsisbio.reports_documents;
DROP TABLE IF EXISTS lafsisbio.roles;
DROP TABLE IF EXISTS lafsisbio.roles_permissions;
DROP TABLE IF EXISTS lafsisbio.technical_team;
DROP TABLE IF EXISTS lafsisbio.users;
DROP TABLE IF EXISTS lafsisbio.users_permissions;
DROP TABLE IF EXISTS lafsisbio.users_roles;

--
-- Name: analyst; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.analyst (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    cpf character varying(255) NOT NULL,
    access character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);



CREATE TABLE lafsisbio.analysts (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    cpf character varying(255) NOT NULL,
    access character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);



CREATE TABLE lafsisbio.area_protegidas (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);



CREATE TABLE lafsisbio.cavernas (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);



CREATE TABLE lafsisbio.documents (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    subtype character varying(255) NOT NULL,
    emitter character varying(255) NOT NULL,
    number character varying(255) NOT NULL,
    dt_create date NOT NULL,
    dt_val date NOT NULL,
    file character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    process_id integer NOT NULL
);




CREATE TABLE lafsisbio.enterprises (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    process character varying(255) NOT NULL,
    sponsor character varying(255) NOT NULL,
    dt_open date NOT NULL,
    cnpj character varying(255) NOT NULL,
    cpf character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    technical_id integer NOT NULL,
    analyst_id integer NOT NULL
);




--
-- Name: enterprises_processes; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.enterprises_processes (
    enterprise_id integer NOT NULL,
    process_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);




--
-- Name: enterprises_technicals; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.enterprises_technicals (
    enterprise_id integer NOT NULL,
    analyst_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: password_resets; Type: TABLE; Schema: lafsisbio; Owner: alfredo
--

CREATE TABLE lafsisbio.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);




--
-- Name: permissions; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.permissions (
    id integer NOT NULL,
    slug character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);




--
-- Name: processes; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.processes (
    id integer NOT NULL,
    sponsor character varying(255) NOT NULL,
    number character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    others integer NOT NULL,
    others_text character varying(255) NOT NULL
);



CREATE TABLE lafsisbio.processes_analysts (
    process_id integer NOT NULL,
    analyst_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);



CREATE TABLE lafsisbio.processes_areas (
    process_id integer NOT NULL,
    area_protegida_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);




--
-- Name: reports_documents; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.reports_documents (
    id integer NOT NULL,
    report_id integer NOT NULL,
    document_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);




--
-- Name: roles; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.roles (
    id integer NOT NULL,
    slug character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);




--
-- Name: roles_permissions; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.roles_permissions (
    role_id integer NOT NULL,
    permission_id integer NOT NULL
);




--
-- Name: technical_team; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.technical_team (
    id integer NOT NULL,
    name integer NOT NULL,
    cpf character varying(255) NOT NULL,
    access character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);






--
-- Name: users; Type: TABLE; Schema: lafsisbio; Owner: alfredo
--

CREATE TABLE lafsisbio.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    sq_usuario bigint,
    sq_usuario_externo bigint
);





CREATE SEQUENCE lafsisbio.users_id_seq START 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: lafsisbio; Owner: alfredo
--

ALTER SEQUENCE lafsisbio.users_id_seq OWNED BY lafsisbio.users.id;


--
-- Name: users_permissions; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.users_permissions (
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);




--
-- Name: users_roles; Type: TABLE; Schema: lafsisbio; Owner: postgres
--

CREATE TABLE lafsisbio.users_roles (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);





--
-- Name: users id; Type: DEFAULT; Schema: lafsisbio; Owner: alfredo
--

ALTER TABLE ONLY lafsisbio.users ALTER COLUMN id SET DEFAULT nextval('lafsisbio.users_id_seq'::regclass);


--
-- Data for Name: analyst; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.analyst (id, name, cpf, access, email, status, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: analysts; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.analysts (id, name, cpf, access, email, status, user_id, created_at, updated_at) FROM stdin;
13		44444				9	2018-11-18 15:49:47	2018-11-18 15:49:47
17	Analyst 22	 		email@email.com		7	2018-11-18 15:58:14	2018-11-18 15:58:14
22	Usama Muneer	 		email@email.com		5	2018-11-19 11:48:22	2018-11-19 11:48:22
23		406091568888				7	2018-11-19 15:34:40	2018-11-19 15:34:40
27	Usuário	 		usuario@provedor.com.br		14	2018-11-21 13:33:03	2018-11-21 13:33:03
29		2				7	2018-11-21 15:45:11	2018-11-21 15:45:11
30		2				7	2018-11-21 15:49:35	2018-11-21 15:49:35
33		1				7	2018-11-21 15:55:38	2018-11-21 15:55:38
34		1				7	2018-11-21 15:59:55	2018-11-21 15:59:55
36		1				7	2018-11-21 16:02:59	2018-11-21 16:02:59
37		2				7	2018-11-21 16:03:02	2018-11-21 16:03:02
38		3				7	2018-11-21 16:03:39	2018-11-21 16:03:39
42		888.333.111-00				7	2018-11-21 16:24:05	2018-11-21 16:24:05
43		222.555.779-98				7	2018-11-21 16:26:04	2018-11-21 16:26:04
44		4				7	2018-11-28 11:00:59	2018-11-28 11:00:59
45		5				7	2018-11-28 11:24:38	2018-11-28 11:24:38
46		6				7	2018-11-28 11:24:43	2018-11-28 11:24:43
47	Matheus	 		matheus@skymarket.com.br		10	2018-11-30 11:47:43	2018-11-30 11:47:43
48		65				7	2018-11-30 15:37:40	2018-11-30 15:37:40
49		123456789				7	2018-12-03 11:05:45	2018-12-03 11:05:45
50		09876544321				7	2018-12-03 11:05:52	2018-12-03 11:05:52
\.


--
-- Data for Name: area_protegidas; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.area_protegidas (id, name, created_at, updated_at) FROM stdin;
1	Interfere em Unidade de Consevação Federal	\N	\N
2	Pesquisa em Unidade de Consevação Federal	\N	\N
3	Afeta cavidades naturais subterrâneas (cavernas)	\N	\N
4	Afeta tartarugas marinhas	\N	\N
\.


--
-- Data for Name: cavernas; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.cavernas (id, name, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: documents; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.documents (id, title, type, subtype, emitter, number, dt_create, dt_val, file, created_at, updated_at, process_id) FROM stdin;
\.


--
-- Data for Name: enterprises; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.enterprises (id, name, process, sponsor, dt_open, cnpj, cpf, email, created_at, updated_at, technical_id, analyst_id) FROM stdin;
1	Ponte do sul	111.111.111.111	Construtora ABC	2018-10-09	429.381.318-04a	6	abc@mail.com	\N	2018-11-15 14:58:28	5	0
2	Metro Norte	222.222.222.222	Construtora XYZ	2010-10-11	123.456.789-10a	7	xyz@mail.com	\N	2018-11-15 14:58:48	5	0
3	Prédio de servidores web	333.333.333.333	Construtora QWERTY	2010-10-11	555.555.555-55a	8	qwerty@mail.com	2018-11-04 22:00:00	2018-11-17 10:09:00	5	0
4	Empresa teste 1	1	Sponsor 1	2010-10-11	1	1	qwerty@mail.com	2018-11-04 22:00:00	2018-11-21 13:01:07	10	0
5	Empresa teste 2	2	Sponsor 2	2010-10-11	2	2	qwerty@mail.com	2018-11-04 22:00:00	2018-11-20 15:46:29	13	0
6	Empresa teste 3	3	Sponsor 3	2010-10-11	3	3	qwerty@mail.com	2018-11-04 22:00:00	2018-11-28 11:22:34	10	0
7	Empresa teste 4	4	Sponsor 4	2010-10-11	4	4	qwerty@mail.com	2018-11-04 22:00:00	2018-11-17 10:09:00	5	0
8	Empresa teste 5	5	Sponsor 5	2010-10-11	5	5	qwerty@mail.com	2018-11-04 22:00:00	2018-11-17 10:09:00	5	0
\.


--
-- Data for Name: enterprises_processes; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.enterprises_processes (enterprise_id, process_id, created_at, updated_at) FROM stdin;
1	61	\N	\N
1	64	\N	\N
2	66	\N	\N
2	67	\N	\N
6	68	\N	\N
8	69	\N	\N
\.


--
-- Data for Name: enterprises_technicals; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.enterprises_technicals (enterprise_id, analyst_id, created_at, updated_at) FROM stdin;
1	29	\N	\N
2	23	\N	\N
2	42	\N	\N
2	43	\N	\N
3	13	\N	\N
3	36	\N	\N
3	37	\N	\N
4	44	\N	\N
4	45	\N	\N
4	46	\N	\N
5	34	\N	\N
6	50	\N	\N
1	22	\N	\N
\.



--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: lafsisbio; Owner: alfredo
--

COPY lafsisbio.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.permissions (id, slug, name, created_at, updated_at) FROM stdin;
1	create-tasks	Create Tasks	2018-11-15 13:41:22	2018-11-15 13:41:22
2	edit-users	Edit Users	2018-11-15 13:41:22	2018-11-15 13:41:22
\.


--
-- Data for Name: processes; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.processes (id, sponsor, number, created_at, updated_at, others, others_text) FROM stdin;
61	Ibama	1	2018-11-18 15:58:41	2018-11-18 15:58:41	1	aaaa
64	Ibama	2	2018-11-19 09:28:32	2018-11-19 09:28:32	1	?
66	Prefeitura	3	2018-11-19 15:35:06	2018-11-19 15:35:06	1	aaaa
67	Ibama	4	2018-11-19 15:52:38	2018-11-19 15:52:38	1	aaaa
68	Ibama	135	2018-12-03 11:04:19	2018-12-03 11:04:19	1	aaaa
69	Ibama	098765432	2018-12-05 14:43:32	2018-12-05 14:43:32	1	aaaa
\.


--
-- Data for Name: processes_analysts; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.processes_analysts (process_id, analyst_id, created_at, updated_at) FROM stdin;
61	17	\N	\N
66	27	\N	\N
66	47	\N	\N
61	22	\N	\N
\.


--
-- Data for Name: processes_areas; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.processes_areas (process_id, area_protegida_id, created_at, updated_at) FROM stdin;
61	1	\N	\N
61	4	\N	\N
64	1	\N	\N
64	2	\N	\N
64	4	\N	\N
66	1	\N	\N
66	2	\N	\N
66	3	\N	\N
66	4	\N	\N
67	1	\N	\N
67	2	\N	\N
67	3	\N	\N
67	4	\N	\N
68	1	\N	\N
68	2	\N	\N
68	3	\N	\N
68	4	\N	\N
69	3	\N	\N
69	4	\N	\N
\.


--
-- Data for Name: reports_documents; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.reports_documents (id, report_id, document_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.roles (id, slug, name, created_at, updated_at) FROM stdin;
1	empreendedor	Empreendedor	2018-11-15 13:41:22	2018-11-15 13:41:22
2	tecnico	Tecnico Ambiental	2018-11-15 13:41:22	2018-11-15 13:41:22
\.


--
-- Data for Name: roles_permissions; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.roles_permissions (role_id, permission_id) FROM stdin;
1	1
2	2
\.


--
-- Data for Name: technical_team; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.technical_team (id, name, cpf, access, email, status, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: lafsisbio; Owner: alfredo
--

COPY lafsisbio.users (id, name, email, password, remember_token, created_at, updated_at, sq_usuario, sq_usuario_externo) FROM stdin;
4	Chris Sevilleja	email2@email.com	$2y$10$zVyDp4BsUgObTVYEpVtJLeKtf/wUt87H//jBBTgJ2YJVexHS8kx.G	\N	2018-11-15 13:42:06	2018-11-15 13:42:06	\N	\N
5	Usama Muneer	email@email.com	$2y$10$bdo5tfgFpyaC7j5D9IEVDO3sGPNqI/JuPYJ0MKNFYgil1Ap0dj1Ne	QTdORgn9FTQtHanfJ6ErYRizeFqhML5n8VcXBZCEil1AG2WkSF66z0t9AZKp	2018-11-15 13:42:06	2018-11-15 13:42:06	\N	\N
6	Asad Butt	asad@thewebtier.com	$2y$10$DZVA0ZqUuM6T.riohXIuoup2D68ma9kE3sWvT8QxzdqZH921pIp0m	\N	2018-11-15 13:42:06	2018-11-15 13:42:06	\N	\N
8	alfredo	alfredo@stairsdigital.com	$2y$10$gcvZlj5muYPY7L9GT5SSsO2N//zklVZeWGy9GbOtdOfDLNhoWlLVe	CVvlVsYy26ZPYDZUhte5spZrDJWAPwfAR2wu2qyWmI6V8S8eq5oZujBqqdxn	2018-10-22 16:39:29	2018-10-22 16:39:29	\N	\N
9	Sergio Rodrigues Morbiolo	sergio@skymarket.com.br	$2y$10$CXRY5w7RSaEQN2mC1nGCVOg0lKx/nE3fO7tGuBr/w9/WwZqu0xbkG	\N	2018-10-22 16:41:18	2018-10-22 16:41:18	\N	\N
10	Matheus	matheus@skymarket.com.br	$2y$10$GstTTyDypN2L3kcxRvO4k.o5dWjBaN37JB9x2kKvIkBJIBeI1l.ui	lCHjBL15kLsa7uP9rEKgtINOpXZSwJD58VdeFk97sssoM1sPigY7snzLMn47	2018-10-22 16:41:39	2018-10-22 16:41:39	\N	\N
11	RAFAEL LUIS FONSECA	rafael@skymarket.com.br	$2y$10$c0QUcqbB0tGEEZJqZ4fo8Osk6WGb/oGPcy7Germ3xz4jKXn6r.baK	\N	2018-10-22 17:02:37	2018-10-22 17:02:37	\N	\N
12	Matheus	guilger.tester@gmail.com	$2y$10$VhD23lLsP1hcGTMNb2FgSucpYqk6Ua7lg8rkPdWjCBGpjae2dkTdG	Jlw8fFM3LQBpoyvdYHx6XMoBWmnlSjpm3yzfvNnqapHPxpv111yU2DIfxeOd	2018-11-20 10:20:15	2018-11-20 10:20:15	\N	\N
13	Eduardo morbiolo	eduardo@skymarket.com.br	$2y$10$z3moO8vhzOjGfYWKa.ZvG.4g32uvryOzPoKpzCbNHy8hIeNodY9wK	mB5pRJa6T6pKWjV1ioQzgHlGpNic9tIneuOznupd7ZdGe2wUmLZmywXh1b9F	2018-11-20 15:37:46	2018-11-20 15:37:46	\N	\N
14	Usuário	usuario@provedor.com.br	$2y$10$Zal1Dr2SnsmlQnIl.n/ueuJWX32DCubziId3f9J3ZaFWmvBzxORjG	\N	2018-11-21 13:28:45	2018-11-21 13:28:45	\N	\N
\.


--
-- Data for Name: users_permissions; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.users_permissions (user_id, permission_id) FROM stdin;
5	1
6	2
\.


--
-- Data for Name: users_roles; Type: TABLE DATA; Schema: lafsisbio; Owner: postgres
--

COPY lafsisbio.users_roles (user_id, role_id) FROM stdin;
5	1
8	1
9	1
10	1
11	1
14	1
6	2
\.



--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: lafsisbio; Owner: alfredo
--

SELECT pg_catalog.setval('lafsisbio.users_id_seq', 1, false);


--
-- Name: analyst analyst_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.analyst
    ADD CONSTRAINT analyst_pkey PRIMARY KEY (id);


--
-- Name: analysts analysts_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.analysts
    ADD CONSTRAINT analysts_pkey PRIMARY KEY (id);


--
-- Name: area_protegidas area_protegidas_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.area_protegidas
    ADD CONSTRAINT area_protegidas_pkey PRIMARY KEY (id);


--
-- Name: cavernas cavernas_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.cavernas
    ADD CONSTRAINT cavernas_pkey PRIMARY KEY (id);


--
-- Name: documents documents_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: enterprises enterprises_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.enterprises
    ADD CONSTRAINT enterprises_pkey PRIMARY KEY (id);


--
-- Name: enterprises_processes enterprises_processes_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.enterprises_processes
    ADD CONSTRAINT enterprises_processes_pkey PRIMARY KEY (enterprise_id, process_id);


--
-- Name: enterprises_technicals enterprises_technicals_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.enterprises_technicals
    ADD CONSTRAINT enterprises_technicals_pkey PRIMARY KEY (enterprise_id, analyst_id);

--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: processes_analysts processes_analysts_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.processes_analysts
    ADD CONSTRAINT processes_analysts_pkey PRIMARY KEY (process_id, analyst_id);


--
-- Name: processes_areas processes_areas_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.processes_areas
    ADD CONSTRAINT processes_areas_pkey PRIMARY KEY (process_id, area_protegida_id);


--
-- Name: processes processes_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.processes
    ADD CONSTRAINT processes_pkey PRIMARY KEY (id);


--
-- Name: reports_documents reports_documents_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.reports_documents
    ADD CONSTRAINT reports_documents_pkey PRIMARY KEY (id);


--
-- Name: roles_permissions roles_permissions_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.roles_permissions
    ADD CONSTRAINT roles_permissions_pkey PRIMARY KEY (role_id, permission_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: technical_team technical_team_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.technical_team
    ADD CONSTRAINT technical_team_pkey PRIMARY KEY (id);



--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: lafsisbio; Owner: alfredo
--

ALTER TABLE ONLY lafsisbio.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users_permissions users_permissions_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: postgres
--

ALTER TABLE ONLY lafsisbio.users_permissions
    ADD CONSTRAINT users_permissions_pkey PRIMARY KEY (user_id, permission_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: alfredo
--

ALTER TABLE ONLY lafsisbio.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: lafsisbio; Owner: alfredo
--

CREATE INDEX password_resets_email_index ON lafsisbio.password_resets USING btree (email);


--
-- Name: users fk_sicae_usuario_externo_sq_usuario_externo; Type: FK CONSTRAINT; Schema: lafsisbio; Owner: alfredo
--

ALTER TABLE ONLY lafsisbio.users
    ADD CONSTRAINT fk_sicae_usuario_externo_sq_usuario_externo FOREIGN KEY (sq_usuario_externo) REFERENCES sicae.usuario_externo(sq_usuario_externo);


--
-- Name: users fk_sicae_usuario_sq_usuario; Type: FK CONSTRAINT; Schema: lafsisbio; Owner: alfredo
--

ALTER TABLE ONLY lafsisbio.users
    ADD CONSTRAINT fk_sicae_usuario_sq_usuario FOREIGN KEY (sq_usuario) REFERENCES sicae.usuario(sq_usuario);


--
-- PostgreSQL database dump complete
--