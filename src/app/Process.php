<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $fillable = array('sponsor', 'number', 'interfere_UC', 'research', 'caves', 'turtles', 'others', 'others_text');

    //para retornar a entidade pai
    public function enterprise()
    {
        return $this->belongsToMany(Enterprise::class,'enterprises_processes');
    }

    public function areas()
    {
        return $this->belongsToMany(AreaProtegida::class,'processes_areas');
    }

    public function analysts()
    {
        return $this->belongsToMany(Analyst::class,'processes_analysts');
    }

    public function documents()
    {
        return $this->belongsToMany(Document::class,'processes_documents');
    }

    public function reports()
    {
        return $this->belongsToMany(Report::class,'processes_reports');
    }

    public function tables()
    {
        return $this->belongsToMany(Table::class,'processes_tables');
    }
}
