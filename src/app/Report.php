<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //protected $fillable = ['title', 'subtitle', 'goal', 'dt_start', 'dt_end'];

    public function documents() {
        return $this->hasMany('App\Document');
    }
}

