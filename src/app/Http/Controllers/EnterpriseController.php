<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Request;
use Auth;
use Illuminate\Foundation\Auth\User;
use \App\Enterprise;
use \App\Analyst;

class EnterpriseController extends Controller
{
    //prevent access without login
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $search = \Request::get('search');  //the param of URI

        $userId = Auth::user()->id;

        $enterprises =
            Enterprise::
            where('name','like','%'.$search.'%')
            ->orWhere('cnpj','like','%'.$search.'%')
            ->orWhere('cpf','like','%'.$search.'%')
            ->orWhere('email','like','%'.$search.'%')
            ->orWhere('process','like','%'.$search.'%')
            ->orderBy('name')
            ->paginate(999);

        foreach($enterprises as $e){

            $technical = User::where('id',$e->technical_id)->take(1)->get();
            foreach($technical as $t) {
                $e->technical_name = $t->name;
            }
        }

        return view('enterprisesList/list',compact('enterprises'));
    }

    public function show($id)
    {
        $enterprise=\App\Enterprise::find($id);

        $technical = User::where('id',$enterprise->technical_id)->take(1)->get();

        foreach($technical as $t) {
            $enterprise->technical_name = $t->name;
        }

        return view('enterprises/list',compact('enterprise'));
    }

    public function create()
    {
        return view('enterprises/create',compact('enterprises'));
    }

    public function store(Request $request)
    {
        // inputs
        $process = $request::input('process');
        $cpf = $request::input('cpf');

        // se houver algum empreendimento no banco com os dados do input (temporario, precisa puxar do sistema externo)
        $processes = \App\Process::where([
            ['number', $process]
        ])->first();

        $enterprise = null;

        if($processes != null){
            if($processes->enterprise->first()->cpf == $cpf){
                $enterprise = $processes->enterprise->first();
            }
        }

        // se encontrar algum dado no banco
        if($enterprise != null) {
            $msg = "O Empreendimento agora está atribuído ao seu usuário.";

            $analyst = Analyst::where('user_id', Auth::user()->id)->first();

            if(is_null($analyst)){
                $user = User::find(Auth::user()->id);
                $analyst = new Analyst();
                $analyst->name = $user->name;
                $analyst->email = $user->email;
                $analyst->cpf = ' ';
                $analyst->user_id = Auth::user()->id;
                $analyst->save();
            };

            $enterprise->technicals()->attach($analyst);

        }else{
            $msg = "Não foi encontrado nenhum empreendimento";
        }

        // volta para lista
        return redirect('enterprises')->with('success', $msg);
    }

    public function destroy($id)
    {
        $enterprise = \App\Enterprise::find($id);
        $enterprise->delete();
        return redirect('enterprises')->with('success','Information has been  deleted');
    }
}
