<?php

namespace App\Http\Controllers;

use App\Analyst;
use App\Enterprise;
use Request;
use App\Permission;
Use App\Role;
Use App\User;
use App\TechnicalTeam;

class TechnicalTeamController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        //prevent access
        return view('home');
    }

    public function show($id){
        $enterprise = Enterprise::find($id);
        return view('technicalTeam/list',compact('enterprise'));
    }

    public function create($id){

        $enterprise = Enterprise::find($id);
        return view('technicalTeam/create',compact('enterprise'));
    }

    public function store(Request $request)
    {
        $params = $request::all();
        $enterprise = Enterprise::find($params['ref']);
        unset($params['ref']);

        $analyst = new Analyst();
        $analyst->name = '';
        $analyst->email = '';
        $analyst->access = '';
        $analyst->status = '';
        $analyst->user_id = 7;
        $analyst->cpf = $params['cpf'];

        $enterprise->technicals()->save($analyst);

        return view('technicalTeam/list',compact('enterprise'))->with('success', 'Analista vinculados com sucesso');
    }

    public static function destroy($id)
    {
        $analyst = Analyst::find($id);

        $enterprise = $analyst->enterprise()->first();

        $analyst->enterprise()->detach();
        $analyst->save();

        return view('technicalTeam/list',compact('enterprise'))->with('success', 'Analista removido com sucesso');

    }

    // novo responsavel tecnico
    public function newRole(Request $request){

        $params = $request::all();
        $tech = Analyst::find($params['ref']);
        $enterprise = $tech->enterprise()->first();

        // atribuir permissao
        $manager_role = Role::where('slug', 'manager')->first();
        $manager_perm = Permission::where('slug','edit-users')->first();

        $user = User::where('email', $params['email'])->get()->first();

       /* // se esse tecnico nao esta na table de users, cria um novo
        if($user == null){
            $user = new User();
            $user->name = 'Nome Temporario';
            $user->email = 'temp@mail.com';
            $user->password = bcrypt('secret');
            $user->save();
        }

        // se nao existe na tabela de relacionamento
        if(!$tech->user()->first()){
            // relacionamento de usuario com tecnico
            $tech->user()->attach($user);
        }

        // retorna a id do usuario da tabela de relacionamento
        $rel = $tech->user()->first();

        // se nao ncontrar nenhum id desse usuario na table de relacionamento, entao cria um novo
        if(!DB::select('select * from users_permissions where user_id = ?', [$rel->id])){
            $user->roles()->attach($manager_role);
            $user->permissions()->attach($manager_perm);
        }*/

        // atualizar pagina para URL correta
        return redirect()->route('technicalTeam.show',compact('enterprise'));
    }
}
