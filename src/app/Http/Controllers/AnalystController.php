<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnalystController extends Controller
{
    //prevent access without login
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $analyst=\App\Analyst::all();
        return view('analyst/list',compact('analyst'));
    }

    public function create()
    {
        return view('analyst/create',compact('analyst'));
    }

    public function edit($id)
    {
        $analyst = \App\Analyst::find($id);
        return view('analyst/edit',compact('analyst','id'));
    }

    public function store(Request $request)
    {
        if($request->hasfile('filename'))
        {
            $file = $request->file('filename');
            $name=time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
        }

        $analyst = new \App\Analyst();
        $analyst->name=$request->get('name');
        $analyst->cpf=$request->get('cpf');
        $date=date_create($request->get('date'));
        $format = date_format($date,"Y-m-d");
        $analyst->date = strtotime($format);
        $analyst->save();

        return redirect('analyst')->with('success', 'Information has been added');
    }

    public function update(Request $request, $id)
    {
        $analyst= \App\Analyst::find($id);
        $analyst->name=$request->get('name');
        $analyst->cpf=$request->get('cpf');
        $analyst->save();
        return redirect('analyst');
    }

    public function destroy($id)
    {
        $analyst = \App\Analyst::find($id);
        $analyst->delete();
        return redirect('analyst')->with('success','Information has been  deleted');
    }
}
