<?php

namespace App\Http\Controllers;

use App\AreaProtegida;
use Request;
use App\Process;
use App\Enterprise;

class ProcessController extends Controller
{
    //prevent access without login
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //prevent access
        return view('home');
    }

    public function show($id){
        $enterprise = Enterprise::find($id);
        return view('process/list',compact('enterprise'));
    }

    public function create($id)
    {
        $enterprise = Enterprise::find($id);
        $enterprise->areas_protegidas = AreaProtegida::all();
        return view('process/create',compact('enterprise'));
    }

    public function edit($id)
    {
        $process = Process::find($id);
        return view('process/edit',compact('process','id'));
    }

    public function store(Request $request)
    {
        $params = $request::all();

        $enterprise = Enterprise::find($params['ref']);

        $process = new Process($params);

        $enterprise->processes()->save($process);

        $areas = AreaProtegida::all();
        $idArea = "";

        foreach ($areas as $a){
            $idArea = $a->id;
            foreach ($params as $p){
                if($p == $idArea){
                    $process->areas()->attach($a);
                    break;
                }
            }
        }

       return view('process/list',compact('enterprise'))->with('success', 'Processo criado com sucesso');
    }

    public function update(Request $request, $id)
    {
        /*
        $enterprise= \App\Process::find($id);
        $enterprise->name=$request->get('name');
        $enterprise->email=$request->get('email');
        $enterprise->number=$request->get('number');
        $enterprise->office=$request->get('office');
        $enterprise->save();
        */
        return view('home');
    }

    public function destroy($id)
    {
        $process = Process::find($id);
        $enterprise = $process->enterprise()->first();
        $process->delete();
        return view('process/list',compact('enterprise'))->with('success', 'Processo removido com sucesso');

    }
}
