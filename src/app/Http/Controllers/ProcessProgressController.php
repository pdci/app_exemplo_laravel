<?php

namespace App\Http\Controllers;

use App\Analyst;
use App\Process;
use Auth;
use Illuminate\Foundation\Auth\User;
use Request;
use App\Enterprise;

class ProcessProgressController extends Controller
{
    //prevent access without login
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $search = \Request::get('search');  //the param of URI

        $enterprises =
            Enterprise::
                where('name','like','%'.$search.'%')
                ->orWhere('cnpj','like','%'.$search.'%')
                ->orWhere('cpf','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%')
                ->orWhere('process','like','%'.$search.'%')
                ->orderBy('name')
                ->paginate(999);

        foreach($enterprises as $e){
            $technical = User::where('id',$e->technical_id)->take(1)->get();

            foreach($technical as $t) {
                $e->technical_name = $t->name;
            }
        }

        return view('processes_progress/list',compact('enterprises'));

    }

    public function update($id)
    {
        $process= Process::find($id);

        $analyst = Analyst::where('user_id', Auth::user()->id)->first();

        if(is_null($analyst)){
            $user = User::find(Auth::user()->id);
            $analyst = new Analyst();
            $analyst->name = $user->name;
            $analyst->email = $user->email;
            $analyst->cpf = ' ';
            $analyst->user_id = Auth::user()->id;
            $analyst->save();
        };

        $process->analysts()->attach($analyst);

        return redirect('progress');
    }

    public function create()
    {
        return view('enterprises/create',compact('enterprises'));
    }
}
