<?php

namespace App\Http\Controllers;

use App\Document;
use Request;
use App\Process;
use \App\Enterprise;


class ReportController extends Controller
{
    //prevent access without login
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function show($id)
    {
        $enterprise= Enterprise::find($id);
        return view('reports/list',compact('enterprise'));
    }

    public function create()
    {
        return view('reports/create',compact('reports'))->with('process', Process::all());
    }

    public function edit($id)
    {
        $reports = \App\Report::find($id);
        return view('reports/edit',compact('reports','id'));
    }

    public function store(Request $request)
    {
        $params = $request::all();
        $report= new \App\Report($params);

        /*
        $report->process_id = $request->get('process_id');
        $report->title = $request->get('title');
        $report->subtitle = $request->get('subtitle');
        $report->goal = $request->get('goal');
        $report->dt_start = $request->get('dt_start');
        $report->dt_end = $request->get('dt_end');
        */

        $report->save();

        return redirect('reports')->with('success', 'Information has been added');
    }

    public function update(Request $request, $id)
    {
        /*
        $enterprise= \App\Process::find($id);
        $enterprise->name=$request->get('name');
        $enterprise->email=$request->get('email');
        $enterprise->number=$request->get('number');
        $enterprise->office=$request->get('office');
        $enterprise->save();
        */
        return redirect('reports');
    }

    public function destroy($id)
    {
        $report = \App\Report::find($id);
        $report->delete();
        return redirect('reports')->with('success','Information has been  deleted');
    }
}
