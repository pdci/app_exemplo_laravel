<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SislicController extends Controller
{
    //prevent access without login
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $host = "http://ws.ibama.gov.br/ws_pnla/ibama/licenciamentos.php";


        $ch = curl_init($host);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $result = curl_exec($ch);

        curl_close($ch);

        $returnObj = json_decode($result,true);

        return view('sislic/list',compact('returnObj'));
    }
}
