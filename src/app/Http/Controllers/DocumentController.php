<?php

namespace App\Http\Controllers;

use App\Document;
use App\Process;
use Illuminate\Http\Request;
use App\Enterprise;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    //prevent access without login
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $documents=\App\Document::all();
        return view('documents/list',compact('documents'));
    }

    public function show($id){
        $enterprise = Enterprise::find($id);
        return view('documents/list',compact('enterprise'));
    }

    public function create($id){
        $enterprise = Enterprise::find($id);
        return view('documents/create',compact('enterprise'));
    }

    public function edit($id)
    {
        $documents = \App\Document::find($id);
        return view('documents/edit',compact('documents','id'));
    }

    public function store(Request $request)
    {
        $process = Process::find($request->get('process_id'));
        $document = new Document();

        if($request->hasfile('filename'))
        {
            $file = $request->file('filename');
            $name = time().$file->getClientOriginalName();
            $document->file = $name;
            $file->move(public_path().'/images/', $name);
        }

        $document->title = $request->get('title');
        $document->type = $request->get('type');
        $document->subtype = $request->get('subtype');
        $document->emitter = $request->get('emitter');
        $document->number = $request->get('number');

        $document->dt_start = $request->get('dt_start');
        $document->dt_end = $request->get('dt_end');

        $process->documents()->save($document);

        $enterprise = $process->enterprise->first();

        return redirect('documents/'.$enterprise['id']);
       // return redirect('documents/list',compact('enterprise'))->with('success', 'Documento adicionado com sucesso');
    }

    public function update(Request $request, $id)
    {
        $document= \App\Document::find($id);
        $document->process_number=$request->get('process_number');
        $document->title=$request->get('title');
        $document->type=$request->get('type');
        $document->subtype=$request->get('subtype');
        $document->emitter=$request->get('emitter');
        $document->number=$request->get('number');
        $date=date_create($request->get('dt_start'));
        $format = date_format($date,"Y-m-d");
        $document->dt_start = strtotime($format);
        $date=date_create($request->get('dt_end'));
        $format = date_format($date,"Y-m-d");
        $document->dt_end = strtotime($format);
        $document->file=$request->get('file');
        $document->save();
        return redirect('documents');
    }

    public function destroy($id)
    {

        $doc = Document::find($id);

        $process = $doc->processes()->first();
        $enterprise = $process->enterprise()->first();

        $doc->delete();

        return view('documents/list',compact('enterprise'))->with('success', 'Documento removido com sucesso');
    }

    public function download($id)
    {
        $doc = Document::find($id);
        return Storage::download('public/images/', $doc->file);
    }
}