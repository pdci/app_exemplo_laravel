<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analyst extends Model
{
    public function userRole() {
        return User::find($this->user_id);
    }

    public function enterprise()
    {
        return $this->belongsToMany(Enterprise::class,'enterprises_technicals');
    }
}
