<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    public function technicals() {
        return $this->belongsToMany(Analyst::class,'enterprises_technicals');
    }

    public function processes() {
        return $this->belongsToMany(Process::class,'enterprises_processes');
    }
}
