<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function processes() {
        return $this->belongsToMany(Process::class,'processes_documents');
    }
}

