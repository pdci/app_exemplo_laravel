@extends('layouts.app')

@section('content')

    <div class="container">
        <br />
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="/enterprises" role="tab" aria-selected="true">Empreendimento</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="/technical" aria-selected="false">Equipe Técnica</a>
            </li>
        </ul>

        <br>

        <a href="/enterprises/list/">Associar Empreendimento</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Responsável</th>
                <th>Empreendimento</th>
                <th>Local</th>
                <th colspan="2">Ação</th>
            </tr>
            </thead>
            <tbody>

            <!-- Ira puxar os empreendimentos com a ID do analista logado -->
            @foreach($enterprises as $ent)
                @php
                    $date=date('Y-m-d', $ent['date']);
                @endphp
                <tr>
                    <td>{{$ent['id']}}</td> <!-- ID do empreendimento -->
                    <td>{{$ent['sponsor']}}</td> <!-- Nome do responsavel/empreendedor -->
                    <td>{{$ent['name']}}</td> <!-- Nome do empreendimento -->
                    <td>{{$ent['local']}}</td> <!-- local do empreendimento -->
                    <td>{{$date}}</td>

                    <td><a href="#" class="btn btn-warning">Visualizar</a></td>
                    <td>
                        <form action="#" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit" disabled>Desvincular</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
