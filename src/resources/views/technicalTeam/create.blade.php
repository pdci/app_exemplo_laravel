@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Novo membro</h2><br/>
        <a href="/technicalTeam/{{$enterprise['id']}}">Voltar para lista</a>

        <br><br>

        <form method="post" action="{{url('technicalTeam')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="cpf">Novo membro da equipe</label>
                <input type="text" class="form-control" name="cpf" placeholder="CPF">
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4" style="margin-top:60px">
                    <input type="text" class="form-check-input" name="ref" value="{{$enterprise['id']}}" hidden="true">
                    <button type="submit" class="btn btn-success">Cadastrar</button>
                </div>
            </div>
        </form>
    </div>

@endsection
