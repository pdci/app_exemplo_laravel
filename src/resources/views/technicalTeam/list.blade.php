@extends('layouts.app')

@section('content')


    <div class="container">
        <br />
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif

        @component('components.tabs')
            @slot('process_title') {{$enterprise['name']}} @endslot
            @slot('enterprise_id') {{$enterprise['id']}} @endslot
            @slot('enterprises')@endslot
            @slot('techinalTeam') active @endslot
            @slot('process') @endslot
            @slot('documents')@endslot
            @slot('reports')@endslot
            @slot('maps')@endslot
            @slot('tables')@endslot
        @endcomponent

        <br>

        <a href="/addanalyst/{{$enterprise['id']}}">Novo técnico</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Perfil de acesso</th>
                    <th>CPF</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Ação</th>
                    <th>Responsável</th>
                </tr>
            </thead>
            <tbody>

            @foreach($enterprise['technicals'] as $tec)
                <tr>
                    <td></td>
                    <td>@if ($tec->user_id != 7  && $tec->userRole() != null) {{$tec->userRole()->name}} @endif</td>
                    <td>{{$tec->cpf}}</td>
                    <td></td>
                    <td>{{$tec->status}}</td>
                    <td>
                        <!-- Para deixar a modal funcional, coloquei a ID do tecnico no nome da modal, precisar mudar isso posteriormente -->
                        <form action="{{action('TechnicalTeamController@destroy', $tec->id)}}" method="post">
                            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#delete{{$tec->id}}Modal">A</button>

                            <div id="delete{{$tec->id}}Modal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Confirmação</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Remover membro "User name" da equipe técnica?</p>
                                        </div>
                                        <div class="modal-footer">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" class="btn btn-danger">Sim</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>

                    <td>
                        <form method="get" action="{{url('/newrole')}}">

                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#confModalDef{{$tec->id}}">
                                Definir</button>

                            <input type="hidden" name="ref" value="{{$tec->id}}">
                            <input type="hidden" name="email" value="{{$tec->email}}">

                            <div id="confModalDef{{$tec->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Confirmação</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Definir {{$tec->name}} como novo responsável técnico?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger">Sim</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
