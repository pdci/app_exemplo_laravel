@extends('layouts.app')

@section('content')

    <div class="container">
        <br />
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif

        @component('components.tabs')
            @slot('process_title') {{$enterprise['name']}} @endslot
            @slot('enterprise_id') {{$enterprise['id']}} @endslot
            @slot('enterprises')@endslot
            @slot('techinalTeam')@endslot
            @slot('process') active @endslot
            @slot('documents')@endslot
            @slot('reports')@endslot
            @slot('maps')@endslot
            @slot('tables')@endslot
        @endcomponent

        <br>

        <a href="/createprocess/{{$enterprise['id']}}">Novo processo</a>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Item</th>
                <th>Número do processo</th>
                <th>Órgão responsável</th>
                <th>Motivos</th>
                <th>Data</th>
                <th colspan="2">Ação</th>
            </tr>
            </thead>
            <tbody>

            @foreach($enterprise['processes'] as $p)
                <tr>
                    <td>{{$p->id}}</td>
                    <td>{{$p->number}}</td>
                    <td>{{$p->sponsor}}</td>
                    <td>

                    @foreach($p->areas as $a)
                       {{$a['name']}} <br>
                    @endforeach
                    @if($p->others == 1) {{$p->others_text}} @endif

                    </td>
                    <td>{{ date('d/m/Y H:i:s', strtotime($p->created_at)) }} </td>

                    <td>
                        <form action="{{action('ProcessController@destroy',$p->id)}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">A</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
