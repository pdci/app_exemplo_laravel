@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Novo processo</h2><br/>
        <a href="/process/{{$enterprise['id']}}">Voltar para lista</a>

        <form method="post" action="{{url('process')}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <lable>Órgão responsável</lable>
                    <select name="sponsor" class="form-control">
                        <option value="Ibama">Ibama</option>
                        <option value="Prefeitura">Prefeitura</option>
                        <option value="Estado">Estado</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="number">Número do processo:</label>
                    <input type="text" class="form-control" name="number">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="cpf">Motivo do processo:</label>

                    @foreach($enterprise['areas_protegidas'] as $a)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="{{$a['id']}}" value="{{$a['id']}}">
                            <label class="form-check-label" for="{{$a['id']}}">{{$a['name']}}</label>
                        </div>
                    @endforeach

                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="others" value="1" onchange="inputState()">
                        <label class="form-check-label" for="others">Outros (especificar)</label>
                        <input type="text" class="form-control" name="others_text" id="others_text" disabled>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-check-input" name="ref" value="{{$enterprise['id']}}" hidden="true">
                    <button type="submit" class="btn btn-success">Concluir</button>
                </div>
            </div>
        </form>
    </div>

    <script>
        function inputState() {
            if(document.querySelector('#others_text').disabled)
                document.querySelector('#others_text').disabled = false;
            else
                document.querySelector('#others_text').disabled = true;
        }
    </script>

@endsection
