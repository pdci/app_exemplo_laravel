@extends('layouts.app')

@section('content')

    <div class="container">
        <br />
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif

        @component('components.tabs')
            @slot('process_title') {{$enterprise['name']}} @endslot
            @slot('enterprise_id') {{$enterprise['id']}} @endslot
            @slot('enterprises') active @endslot
            @slot('techinalTeam')@endslot
            @slot('process')@endslot
            @slot('documents')@endslot
            @slot('reports')@endslot
            @slot('maps')@endslot
            @slot('tables')@endslot
        @endcomponent

        <br>

        <div class="card" style="width: 65rem;">
            <div class="card-body">
                <h3 class="card-text">{{$enterprise['name']}}</h3>
                <hr>

                <div class="row">
                    <div class="col-md-2">
                        <h5 class="card-text">Empreendedor:</h5>
                    </div>
                    <div>{{$enterprise['sponsor']}}</div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <h5 class="card-text">CNPJ:</h5>
                    </div>
                    <div>{{$enterprise['cnpj']}}</div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <h5 class="card-text">Responsável Técnico:
                            {{$enterprise['technical_name'] == null ?
                              'Não atribuido' : $enterprise['technical_name']}}
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <h5 class="card-text">CPF:</h5>
                    </div>
                    <div>{{$enterprise['cpf']}}</div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <h5 class="card-text">Analista:
                            {{$enterprise['technical_name'] == null ?
                             'Não atribuido' : $enterprise['technical_name']}}
                        </h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h5 class="card-text">Setor:</h5>
                    </div>
                    <div></div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h5 class="card-text">Tipologia:</h5>
                    </div>
                    <div></div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h5 class="card-text">Data da abertura:</h5>
                    </div>
                    <div>{{ date('d/m/Y', strtotime($enterprise['dt_open'])) }} </div>
                </div>

                <div class="row">
                    <div class="col-md-1">
                        <h5 class="card-text">Email:</h5>
                    </div>
                    <div>{{$enterprise['email']}}</div>
                </div>

            </div>
        </div>
    </div>

@endsection
