@extends('layouts.app')

@section('content')

<div class="container">
    <h2>Novo Empreendimento</h2><br/>
    <a href="/enterprises">Voltar para lista</a>

    <form method="post" action="{{url('enterprises')}}" enctype="multipart/form-data" id="form">
        @csrf
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="process">Número do processo:</label>
                <input type="text" class="form-control" name="process" required>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="cpf">CPF do responsavel:</label>
                <input type="text" class="form-control" name="cpf" required>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success">Atribuir Empreendimento</button>
            </div>
        </div>
    </form>
</div>

@endsection
