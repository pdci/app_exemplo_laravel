<!-- edit.blade.php -->
@extends('layouts.app')

@section('content')

<div class="container">
    <h2>Editar Técnico</h2><br  />
    <form method="post" action="{{action('EnterpriseController@update', $id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Nome:</label>
                <input type="text" class="form-control" name="name" value="{{$enterprise->name}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" value="{{$enterprise->email}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="number">Telefone:</label>
                <input type="text" class="form-control" name="number" value="{{$enterprise->number}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-left:38px">
                <lable>Local</lable>
                <select name="office">
                    <option value="São Paulo"  @if($enterprise->office=="São Paulo") selected @endif>São Paulo</option>
                    <option value="Rio de Janeiro"  @if($enterprise->office=="Rio de Janeiro") selected @endif>Rio de Janeiro</option>
                    <option value="Amazonas" @if($enterprise->office=="Amazonas") selected @endif>Amazonas</option>
                    <option value="Paraná" @if($enterprise->office=="Paraná") selected @endif>Paraná</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Atualizar</button>
            </div>
        </div>
    </form>
</div>

@endsection
