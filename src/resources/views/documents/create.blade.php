@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Novo Documento</h2><br/>
        <a href="/documents/{{$enterprise['id']}}">Voltar para lista</a>

        <br>

        <form method="post" action="{{url('documents')}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="process_id">Número do processo:</label>
                    <select name="process_id" class="form-control" required>
                        <option value="" disabled selected>Processo</option>
                        @foreach($enterprise['processes'] as $entPro)
                            <option value="{{$entPro->id}}">{{$entPro->number}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="title">Titúlo do documento:</label>
                    <input type="text" class="form-control" name="title">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="type">Tipo:</label>
                    <select name="type" class="form-control">
                        <option value="Documento de origem">Documento de origem</option>
                        <option value="Requerimentos">Requerimentos</option>
                        <option value="Licenças">Licenças</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="subtype">Subtipo:</label>
                    <select name="subtype" class="form-control">
                        <option value="Referência">Referência</option>
                        <option value="Licença prévia">Licença prévia</option>
                        <option value="Ofício">Ofício</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="emitter">Orgão emitente:</label>
                    <select name="emitter" class="form-control">
                        <option value="Ibama">Ibama</option>
                        <option value="Prefeitura">Prefeitura</option>
                        <option value="Ong">Ong</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="number">Número do documento:</label>
                    <input type="text" class="form-control" name="number">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="dt_start">Data de emissão:</label>
                    <input type="date" class="date form-control" name="dt_start">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="dt_end">Data de validade:</label>
                    <input type="date" class="date form-control" name="dt_end">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <input type="file" name="filename">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4" style="margin-top:60px">
                    <input type="hidden" class="form-check-input" name="ref" value="{{$enterprise['id']}}">
                    <button type="submit" class="btn btn-success">Concluir</button>
                </div>
            </div>
        </form>
    </div>

@endsection
