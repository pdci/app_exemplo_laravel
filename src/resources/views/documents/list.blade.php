@extends('layouts.app')

@section('content')


    <div class="container">
        <br />
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif

        @component('components.tabs')
            @slot('process_title') {{$enterprise['name']}} @endslot
            @slot('enterprise_id') {{$enterprise['id']}} @endslot
            @slot('enterprises')@endslot
            @slot('techinalTeam')@endslot
            @slot('process')@endslot
            @slot('documents') active @endslot
            @slot('reports')@endslot
            @slot('maps')@endslot
            @slot('tables')@endslot
            @slot('history')@endslot
        @endcomponent

        <br><br>

        <div class="offset-3">
            <label>Processo</label>
            <select class="col-md-3" name="process_id" disabled>
                <option value="" disabled selected>Nº do processo</option>
                @foreach($enterprise->processes()->get() as $pro)
                    <option value="{{$pro['id']}}">{{$pro['number']}}</option>
                @endforeach
            </select>
        </div>

        <br>

        <h2>Documentos vinculados ao processo</h2>
        <a href="/createdocument/{{$enterprise['id']}}">Novo documento</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Item</th>
                <th>Número do documento</th>
                <th>Título</th>
                <th>Tipo</th>
                <th>Subtipo</th>
                <th>Emitente</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>

            @foreach($enterprise->processes()->get() as $ent)

                    @foreach($ent->documents()->get() as $doc)
                        <tr>
                            <td>{{$doc->id}}</td>
                            <td>{{$doc->number}}</td>
                            <td>{{$doc->title}}</td>
                            <td>{{$doc->type}}</td>
                            <td>{{$doc->subtype}}</td>
                            <td>{{$doc->emitter}}</td>

                            <td>
                                <a href="{{ route('dd', $doc->id)  }}" class="btn btn-success">Baixar</a>
                            </td>
                            <td>
                                <form action="{{action('DocumentController@destroy', $doc->id)}}" method="post">
                                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#delete{{$doc->id}}Modal">Apagar</button>

                                    <div id="delete{{$doc->id}}Modal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmação</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Remover documento "{{$doc->title}}" ? Essa ação não poderá ser desfeita.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button type="submit" class="btn btn-danger">Sim</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
            @endforeach

            </tbody>
        </table>
    </div>

@endsection
