@extends('layouts.app')

@section('content')

<div class="container">
    <br />
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif


    <br>

    <h1>Processos em Andamento</h1>

    <br><br>

    {!! Form::open(['method'=>'GET','url'=>'/progress','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

    <div class="input-group custom-search-form">
        <input type="text" class="form-control" name="search" placeholder="Buscar processo em andamento...">
        <span class="input-group-btn">
                <button class="btn btn-default-sm" type="submit">
                    <i class="fa fa-search">Buscar
                </button>
            </span>
    </div>

    {{ Form::close() }}

    @foreach($enterprises as $e)

        @php $s = false; @endphp
        @foreach($e->technicals as $t)
            @if($t->user_id == auth()->user()->id)
                @php $s = true; @endphp
            @endif
        @endforeach

        @if($s)
            @foreach($e->processes as $p)
                <br>

                <div class="card" style="width: 65rem;">
                    <div class="card-body">
                            <h3 class="card-text"><a href="enterprises/{{$e['id']}}">{{$e['name']}}</a></h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="card-text">Processo nº:</h5>
                            </div>
                            <div>{{$p->number}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="card-text">Empreendedor:</h5>
                            </div>
                            <div>{{$p['sponsor']}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="card-text">CNPJ:</h5>
                            </div>
                            <div>{{$e['cnpj']}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="card-text">Data da abertura:</h5>
                            </div>
                            <td>{{ date('d/m/Y H:i:s', strtotime($p->created_at)) }} </td>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <h5 class="card-text">Analista: <b>
                                            @php
                                                //Para colocar virgulas corretamente
                                                $arranalysts = [];
                                                $isanalyst = false;
                                            @endphp
                                            @if(!$p->analysts->isEmpty())
                                                @foreach($p->analysts as $a)
                                                   @if($a->user_id == auth()->user()->id) @php $isanalyst = true @endphp @endif
                                                   @php array_push($arranalysts,$a->name) @endphp
                                                @endforeach
                                                    @php echo implode( ", ", $arranalysts) @endphp
                                            @else
                                               Não atribuido
                                            @endif

                                        </b>
                                </h5>
                            </div>
                            <div>

                                @if(!$isanalyst)
                                <form action="{{action('ProcessProgressController@update', $p->id)}}" method="post">
                                    <button class="btn btn-default" type="button" data-toggle="modal" data-target="#delete{{$p->id}}Modal">Atribuir para mim</button>

                                    <div id="delete{{$p->id}}Modal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmação</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Confirma a atribuição do processo "{{$p['number']}}" a você?</p>

                                                </div>
                                                <div class="modal-footer">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="PATCH">
                                                    <button type="submit" class="btn btn-danger">Sim</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    @endforeach

</div>

@endsection
