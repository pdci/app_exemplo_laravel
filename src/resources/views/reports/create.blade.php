@extends('layouts.app')

@section('content')

    <?php
    $documents = DB::table('documents')->get();
    ?>

    <div class="container">
        <h2>Novo relatório</h2><br/>
        <a href="/reports/">Voltar para lista</a>

        <form method="post" action="{{url('reports')}}" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label>Processo</label>
                    <select name="process_id" class="form-control">
                        <option value="" disabled selected>Nº do processo</option>
                        @foreach($process as $p)
                            <option value="{{$p['id']}}">{{$p['number']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="title">Título</label>
                    <input type="text" class="form-control" name="title">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="subtitle">Subtítulo</label>
                    <input type="text" class="form-control" name="subtitle">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <lable>Objetivo</lable>
                    <select name="goal" class="form-control">
                        <option value="Levantamento">Levantamento</option>
                        <option value="Monitoramento">Monitoramento</option>
                        <option value="Resgate">Resgate</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="dt_start">Data de emissão:</label>
                    <input type="date" class="date form-control" name="dt_start">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="dt_end">Data de validade:</label>
                    <input type="date" class="date form-control" name="dt_end">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-8">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Nº</th>
                            <th>Título</th>
                            <th>Tipo</th>
                            <th>Subtipo</th>
                            <th>Emitente</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($documents as $d)
                            @php
                                @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" class="form-check-input" name="documents[]" value="{{$d->id}}">
                                </td>
                                <td>{{$d->number}}</td>
                                <td>{{$d->title}}</td>
                                <td>{{$d->type}}</td>
                                <td>{{$d->subtype}}</td>
                                <td>{{$d->emitter}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-success">Cadastrar</button>
                </div>
            </div>
        </form>
    </div>

@endsection
