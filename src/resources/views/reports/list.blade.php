@extends('layouts.app')

@section('content')


    <div class="container">
        <br />
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif

        @component('components.tabs')
            @slot('process_title') {{$enterprise['name']}} @endslot
            @slot('enterprise_id') {{$enterprise['id']}} @endslot
            @slot('enterprises')@endslot
            @slot('techinalTeam')@endslot
            @slot('process')@endslot
            @slot('documents')@endslot
            @slot('reports') active @endslot
            @slot('maps')@endslot
            @slot('tables')@endslot
        @endcomponent

        <br>

        <div class="offset-3">
            <label>Processo</label>
            <select class="col-md-3">
                <option value="" disabled selected>Nº do processo</option>
                @foreach($enterprise->processes as $p)
                    <option value="{{$p['id']}}">{{$p['number']}}</option>
                @endforeach
            </select>
        </div>

        <br>

        <a href="/reports/create/">Novo relatório</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Item</th>
                <th>Título</th>
                <th>Objetivo</th>
                <th>Ínicio</th>
                <th>Fim</th>
                <th colspan="2">Ação</th>
            </tr>
            </thead>
            <tbody>

            {{--@foreach($reports as $r)
                @php
                    @endphp
                <tr>
                    <td>{{$r['id']}}</td>
                    <td>{{$r['title']}}</td>
                    <td>{{$r['goal']}}</td>
                    <td>{{$r['dt_start']}}</td>
                    <td>{{$r['dt_end']}}</td>

                    <td>
                        <form action="{{action('ReportController@destroy', $r['id'])}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">A</button>
                        </form>
                    </td>
                </tr>
            @endforeach--}}
            </tbody>
        </table>
    </div>

@endsection
