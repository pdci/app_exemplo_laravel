@extends('layouts.app')

@section('content')

    <div class="container">
        <br />
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif

        <br>

        <h1>Empreendimentos</h1>

        <a href="/enterprises/create">Atribuir Empreendimento</a>

        <br><br>

        {!! Form::open(['method'=>'GET','url'=>'/enterprises','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

        <div class="input-group custom-search-form">
            <input type="text" class="form-control" name="search" placeholder="Buscar empreendimento...">
            <span class="input-group-btn">
                <button class="btn btn-default-sm" type="submit">
                    <i class="fa fa-search">Buscar
                </button>
            </span>
        </div>

        {{ Form::close() }}

        <br>



        @foreach($enterprises as $e)

            @php $s = false; @endphp
            @foreach($e->technicals as $t)
                @if($t->user_id == auth()->user()->id)
                    @php $s = true; @endphp
                @endif
            @endforeach

            @if($s)
                <br>

                <div class="card" style="width: 65rem;">
                    <div class="card-body">
                        <h3 class="card-text"><a href="/enterprises/{{$e['id']}}">{{$e['name']}}</a></h3>

                        <hr>

                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="card-text">Empreendedor:</h5>
                            </div>
                            <div>{{$e['sponsor']}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <h5 class="card-text">CNPJ:</h5>
                            </div>
                            <div>{{$e['cnpj']}}</div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <h5 class="card-text">Responsável Técnico:
                                    {{$e['technical_name'] == null ?
                                      'Não atribuido' : $e['technical_name']}}
                                </h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <h5 class="card-text">CPF:</h5>
                            </div>
                            <div>{{$e['cpf']}}</div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="card-text">Data da abertura:</h5>
                            </div>
                            <div>{{ date('d/m/Y', strtotime($e['dt_open'])) }} </div>

                        </div>

                        <div class="row">
                            <div class="col-md-1">
                                <h5 class="card-text">Email:</h5>
                            </div>
                            <div>{{$e['email']}}</div>
                        </div>

                    </div>
                </div>
            @endif

        @endforeach

    </div>

@endsection

