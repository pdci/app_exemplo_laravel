<div class="text-right">
    Usuário logado como <b>{{auth()->user()->name }}</b>


</div>


<br>

<h1 class="text-center">{{$process_title}}</h1>

<br>

<div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{$enterprises}}" href="/enterprises/{{$enterprise_id}}" role="tab" aria-selected="true">Empreendimento</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{$techinalTeam}}" href="/technicalTeam/{{$enterprise_id}}" aria-selected="false">Equipe Técnica</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{$process}}" href="/process/{{$enterprise_id}}">Processos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{$documents}}" href="/documents/{{$enterprise_id}}">Documentos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{$reports}}" href="/reports/{{$enterprise_id}}">Relatórios</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{$maps}}" href="#">Mapas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{$tables}}" href="#">Tabelas</a>
        </li>
    </ul>
</div>
